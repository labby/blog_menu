## Blog Menu
Code snippet Blog Menu 1.3.0 for [LEPTON CMS][1], v7.x
### Licensed
GNU Genera Public License
### Authors
- Written by Erik Coenjaerts (Eki)
- Updated by Dietrich Roland Pehlke (Aldus)
***
### SHORT INSTALLATION GUIDE:
- Download the Blog Menu zip file from [LEPADOR][2] repository.
- Log into the backend your LEPTON-CMS installation and install the module as usual.

***
### USING THE SNIPPET FUNCTION:
Once the module is installed, it can be invoked either from the index.php file of your template or from any code module section.

From template index.php:
```php
<?php  
    display_blog_menu(2);  
?>
```
From a code module:
```php
    display_blog_menu(2);
```
***
### ADDITIONAL PARAMETERS
For a more customised output, you can pass over several parameters to the function explained below.

``` php
    display_blog_menu($page_id, $date_option, $group_header, $history_header);
```
***
### Optional Parameters
| Name             | Description                                                                                                   |
|------------------|---------------------------------------------------------------------------------------------------------------|
| *page_id*        | The page_id of the news page you want to generate the Blog Menu from                                          |
| *date_option*    | 0 = Count articles according the posted date (default);<br /> 1 = Count articles according the published date |
| *group_header*   | Header above the group menu (default: `'<h1>Categories</h1>'`)                                                |
| *history_header* | Header above the history menu (default: `'<h1>History</h1>'`)                                                 |
| *display_option* | 0 = Show both history and categories (default);<br /> 1 = Show only history;<br/> 2 = Show only categories    |


#### Example for customised call:
``` php
<?php
    display_blog_menu(2, 1, '<h2>Categories</h2>', '<h2>History</h2>', 2);
?>
```

``` php
    // get a valid page_id
    $iNewsPageID = $database->get_one(  
        "SELECT `page_id` FROM `".TABLE_PREFIX."sections` WHERE `module`='news' LIMIT 1"  
    );

    // get singleton instance
    $oBLOG = blog_menu::getInstance();
    
    // some example settings ...
    $oBLOG->history_header = "<h4 class='blog_menu_h'>History</h4>";
    $oBLOG->group_header   = "<h4 class='blog_menu_h'>Group</h4>";
    $oBLOG->display_option = blog_menu::DISPLAY_BOTH;

    // output
    $blog_menu = $oBLOG->display($iNewsPageID);
    
    echo $blog_menu;
```
        
***
### TROUBLE SHOOTING
- pass over at least the first argument ($page_id)
- mask text with "your text" or 'your text'
- within your template index.php file use: <?php display_blog_menu(); ?>
- within a code module use: display_blog_menu();
- remind the ';' at the end of the code line

***
### STYLE THE OUTPUTS ACCORDING YOUR NEEDS
The output can be customized to your needs without touching the code itself, by the use of CSS definitions.  
Open a code page and enter the following code:
```php
    echo "<div id=\"My-Weblog\">";
        display_blog_menu(2);
    echo "</div>";
```

The additional div section is used to restrict the style definitions to the news only, without influencing  
the rest of your layout. Any CSS styles need to be added to the CSS file of your template.

[1]: https://www.lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com "LEPADOR"