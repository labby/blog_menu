<?php

declare(strict_types=1);

/**
 * @module      blog_menu
 * @author      Erik Coenjaerts, Dietrich Roland Pehlke
 * @copyright   Erik Coenjaerts, Dietrich Roland Pehlke
 * @license     http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

class blog_menu extends LEPTON_abstract
{
    const DISPLAY_GROUP     = 1;
    const DISPLAY_HISTORY   = 2;
    const DISPLAY_BOTH      = self::DISPLAY_GROUP + self::DISPLAY_HISTORY;
    
    const USE_POST_WHEN      = 1;
    const USE_PUBLISHED_WHEN = 2;
    
    public $group_header    = "Categories";
    public $history_header  = 'History';
    public $display_option  = self::DISPLAY_BOTH;
    public $date_option     = self::USE_POST_WHEN;
    
    public $oTWIG = NULL;
    
    public static $instance;
    
    public function initialize()
    {
        $this->group_header     = $this->language['header_group'];
        $this->history_header   = $this->language['header_history'];
        
        $this->oTWIG = lib_twig_box::getInstance();
        $this->oTWIG->registerModule("blog_menu");
        $this->oTWIG->registerPath(LEPTON_PATH."/templates/".DEFAULT_TEMPLATE."/frontend/blog_menu/", "blog_menu");
    }
    
    /**
     *
     *
     * @param int    $page_id        A valid page_id.
     * @param int    $date_option    Optional: 'Use Posted when'
     * @param string $group_header   Optional the header string.
     * @param string $history_header Optional the history header string.
     * @param int    $display_option Optional - display both (3)
     *
     * @return string The generated html-source (via template).
     * 
     */
    public function display(
        $page_id,
        $date_option    = NULL, // optional
        $group_header   = NULL, // '<h1>Categories</h1>' 
        $history_header = NULL, // '<h1>History</h1>'
        $display_option = NULL  // 0 
        ): string
        {
		
        $page_id = intval($page_id);
        if (0 === $page_id)
        {
            echo "";
            return false;
        }
        
        $date_option    = $date_option ?? $this->date_option;
        $group_header   = $group_header ?? $this->group_header;
        $history_header = $history_header ?? $this->history_header;
        $display_option = $display_option ?? $this->display_option;     
        
        $output = "";
        
        // show categories
		if (($display_option & self::DISPLAY_GROUP) == self::DISPLAY_GROUP)
		{ 
            $output = $this->getCategories($page_id);
		}
		
		//show history
		if (($display_option & self::DISPLAY_HISTORY) == self::DISPLAY_HISTORY)
		{ 
            $output .= $this->getHistory($page_id);
		}

        return $output;
	}
	
	public function getCategories(int $page_id): string
	{
        // get link to the page
		$page_link = $this->getPageLink($page_id);
        
        if (NULL === $page_link)
        {
            echo "*";
            return false;
        }
        
        // query to obtain categories for the selected page
        $aAllNews = [];
        
        $database = LEPTON_database::getInstance();
        
        $bSuccess = $database->execute_query(
            "SELECT * FROM `" .TABLE_PREFIX ."mod_news_groups` WHERE `page_id`=".$page_id." AND `active`=true ORDER BY position;",
            true,
            $aAllNews,
            true
        );
		
		if (!empty($aAllNews))
		{
		    if ($this->group_header != "")
		    {
			    $output = $this->group_header;
            }
		}
		
		foreach ($aAllNews as &$group)
		{
            $group["num_of_entries"] = $database->get_one(
                "SELECT count(`post_id`) FROM `" .TABLE_PREFIX ."mod_news_posts` WHERE `page_id`=".$page_id." 
                    AND `active`=true AND `group_id`=".$group['group_id']
            );       
        }
	    
	    return $this->oTWIG->render(
	        "@blog_menu/view_categories.lte",
	        [
	            "allNews"   => $aAllNews,
	            "page_link" => $page_link,
	            "header"    => $this->group_header
	            
	        ]
	    );
    }
	
	public function getHistory(int $page_id): string
	{
	    $page_link = $this->getPageLink($page_id);
	    
	    // determine sorting method
        switch($this->date_option)
        {
            case self::USE_POST_WHEN:
                $date = "posted_when";
                break;
            
            case self::USE_PUBLISHED_WHEN:
	            $date = "published_when";
	            break;
        }
        
        // query to obtain history per month for the selected page
	    $query = "
	        SELECT
	            MONTHNAME(FROM_UNIXTIME(".$date.")) as mo, 
	            MONTH(FROM_UNIXTIME(".$date.")) as m,
	            FROM_UNIXTIME(".$date.",'%Y') as y,
	            COUNT(*) as total
	        FROM
	            `" .TABLE_PREFIX ."mod_news_posts` 
	        WHERE
	                `page_id`=".$page_id." 
	            AND `active`=true 
	        GROUP BY
	            y,m,mo
	        ORDER BY 
	            y DESC,
                m DESC;
	    ";
	    
	    $aAllNews = [];
	    $bSuccess = LEPTON_database::getInstance()->execute_query(
	        $query,
	        true,
	        $aAllNews,
	        true
	    );

        return $this->oTWIG->render(
	        "@blog_menu/view_history.lte",
	        [
	            "date_options"  => $this->date_option,
	            "allNews"       => $aAllNews,
	            "page_link"     => $page_link,
	            "header"        => $this->history_header,
	            "oBlogMenu"     => $this
	        ]
	    );
	}

	/**
	 *
	 * @param int $page_id  A valid page id.
	 *
	 * @return string|bool  A generated string/path or 'false'
	 *
	 */
	public function getPageLink(int $page_id): string|bool
	{
	    // get link to the page
		$page_link = LEPTON_database::getInstance()->get_one(
		    "SELECT `link` FROM `".TABLE_PREFIX."pages` WHERE `page_id`=".$page_id
		);
        
        if (NULL === $page_link)
        {
            return false;
        } else {
            return LEPTON_URL.PAGES_DIRECTORY .$page_link .PAGE_EXTENSION;
        }
	}

}