<?php

/**
 * @module      blog_menu
 * @author      Erik Coenjaerts, Dietrich Roland Pehlke
 * @copyright   Erik Coenjaerts, Dietrich Roland Pehlke
 * @license     http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

$MOD_BLOG_MENU = [
    "header_group"      => "Group",
    "header_history"    => "History"
];
