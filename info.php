<?php

/**
 * @module      blog_menu
 * @author      Erik Coenjaerts, Dietrich Roland Pehlke
 * @copyright   Erik Coenjaerts, Dietrich Roland Pehlke
 * @license     http://www.gnu.org/licenses/gpl-3.0.html
 *
 */

$module_directory   = 'blog_menu';
$module_name        = 'Blog Menu';
$module_function    = 'snippet';
$module_version     = '1.3.1';
$module_platform    = '7.1.0';
$module_author      = 'Erik Coenjaerts, Dietrich Roland Pehlke';
$module_license     = 'GNU General Public License';
$module_description = 'Snippet to display a blog-menu containing active groups and history per month of the news module. Call it by using function: display_blog_menu($page_id,$date_option,$group_header,$history_header,$display_option). For more information, see the included README';
$module_guid        = '50F9E18B-3603-414A-82DB-001E180254EF';
